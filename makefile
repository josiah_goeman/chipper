build: player.o
	g++ -std=c++11 -o build player.o -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system -lncurses

player.o:	player.cpp
	g++ -std=c++11 -c player.cpp

clean:
	-rm build *.o
