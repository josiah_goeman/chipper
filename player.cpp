#include <stdint.h>
#include <iostream>
#include <fstream>
#include <map>
#include <chrono>
#include <thread>
#include <time.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <ncurses.h>
#include "chip8core.hpp"

using namespace std;
using namespace sf;


int romLength;
uint8_t* romBuffer;
Chip8 chip;

void printRegisterBox(int row, int col)
{
	move(row, col);
	printw("+REGISTERS+");
	for(int i = 0; i < 16; i++)
	{
		move(row+i+1, col);
		printw("|V%X:    %02X|", i, chip.v[i]);
	}
	move(row+17, col);
	printw("+---------+");
	move(row+18, col);
	printw("|ip:  %04X|", chip.ip);
	move(row+19, col);
	printw("|sp:    %02X|", chip.sp);
	move(row+20, col);
	printw("|i:   %04X|", chip.address);
	move(row+21, col);
	printw("|dt:    %02X|", chip.dt);
	move(row+22, col);
	printw("|st:    %02X|", chip.st);
	move(row+23, col);
	printw("+---------+");
}

void printStackBox(int row, int col)
{
	move(row, col);
	printw("+STACK+");
	for(int i = 0; i < 16; i++)
	{
		move(row+i+1, col);
		int stackIndex = 15 - i;
		printw("| %04X|", chip.stack[stackIndex]);
		if(stackIndex == chip.sp) printw("<-sp(%d)", stackIndex);
	}
	move(row+17, col);
	printw("+-----+");
}

const int opsPerColumn = 32;
void printProgramBox(int row, int col)
{
	for(int i = 0; i <5+ romLength/2 / opsPerColumn; i++)
		for(int j = 0; j < opsPerColumn*2; j+=2)
		{
			move(row+j/2, col+i*6);
			int index = i*opsPerColumn+j;
			if(index >= romLength) return;
			printw("%02X%02X", romBuffer[index], romBuffer[index+1]);
		}
}

void printInfo()
{
	clear();
	printRegisterBox(0, 0);
	printStackBox(0, 15);
	//printProgramBox(1, 32);
	move(20, 15);
	printw("prev op: %04X", chip.opcode);
	move(21, 15);
	printw("next op: %04X", chip.nextOpcode);
	refresh();
}

int main(int argc, char* argv[])
{
	//set up key mapping
	map<uint8_t, uint8_t> keyMap;
	//keycode		index	key
	keyMap[27] =	0x1;	//1
	keyMap[28] =	0x2;	//2
	keyMap[29] =	0x3;	//3
	keyMap[30] =	0xC;	//4
	keyMap[16] =	0x4;	//q
	keyMap[22] =	0x5;	//w
	keyMap[4] =		0x6;	//e
	keyMap[17] = 	0xD;	//r
	keyMap[0] = 	0x7;	//a
	keyMap[18] = 	0x8;	//s
	keyMap[3] = 	0x9;	//d
	keyMap[5] = 	0xE;	//f
	keyMap[25] = 	0xA;	//z
	keyMap[23] = 	0x0;	//x
	keyMap[2] = 	0xB;	//c
	keyMap[21] = 	0xF;	//v

	if(argc == 1)
	{
		cout << "ROM image is required." << endl;
		return 0;
	}

	//try to find file length
	ifstream romStream(argv[1], ios::binary);
	romStream.seekg(0, romStream.end);
	romLength = romStream.tellg();
	romStream.seekg(0, romStream.beg);

	//file does not exist
	if(romLength == -1)
	{
		cout << "ROM \"" << argv[1] << "\" does not exist." << endl;
		return 0;
	}

	cout << "Loading " << romLength << " bytes from " << argv[1] << "..." << endl;
	romBuffer = new uint8_t[romLength];
	romStream.read(reinterpret_cast<char*>(romBuffer), romLength);

	//did rom load successfully?
	if(romStream)
		cout << "Done." << endl;
	else
	{
		cout << "Read error." << endl;
		return 1;
	}

	srand(time(NULL));

	//initialize interpreter
	chip = Chip8();
	chip.loadRom(romBuffer, romLength);

	//set up audio
	const unsigned AMPLITUDE = 30000;
	
	sf::Int16 toneSamples[44100];

	const float TWO_PI = 6.28318;
	const float increment = 440.0/44100;
	float x = 0;
	for (int i = 0; i < 44100; i++) {
		toneSamples[i] = AMPLITUDE * sin(x*TWO_PI);
		x += increment;
	}
	
	SoundBuffer buffer;
	buffer.loadFromSamples(toneSamples, 44100, 1, 44100);

	Sound sound;
	sound.setBuffer(buffer);
	sound.setLoop(true);

	//set up window
	const uint16_t pixelCount = 64*32*4;
	const uint8_t backgroundColor[4]{100, 100, 100, 255};
	const uint8_t litColor[4]{100, 255, 100, 255};
	uint8_t scale = 10;
	RenderWindow window(sf::VideoMode(64*scale, 32*scale), "Chipper");
	Texture texture;
	texture.create(64, 32);
	Sprite sprite;
	sprite.setTexture(texture);
	sprite.scale(scale, scale);
	uint8_t pixels[pixelCount];

	/*for(int i = 0; i < 64*32; i++)
	{
		chip.frameBuffer[i] = ((i/64)+(i%64))%2==0;
	}*/

	initscr();
	printInfo();

	bool running = true;
	bool step = false;

	while(window.isOpen())
	{
		step = false;

		//events are stored in a stack; we process them all if there are more than one
		Event event;
		while(window.pollEvent(event))
		{
			if(event.type == Event::Closed)
				window.close();

			if(event.type == Event::KeyPressed && keyMap.count(event.key.code))
				chip.keypad[keyMap[event.key.code]] = true;
			if(event.type == Event::KeyReleased && keyMap.count(event.key.code))
				chip.keypad[keyMap[event.key.code]] = false;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::Space)
				running = !running;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::Return)
				step = true;
		}

		/*for(int i = 0; i < 16; i++)
			cout << (chip.keypad[i] ? 1 : 0);
		cout << endl;*/

		if(running || step)
		{
			//getch();
			chip.tick();
			printInfo();
		}

		if(chip.drawFlag)
		{
			//display the framebuffer using chosen colors
			for(int i = 0; i < pixelCount; i += 4)
				memcpy(pixels+i, &(chip.frameBuffer[i/4] ? litColor : backgroundColor), 4);

			texture.update(pixels);
			window.draw(sprite);
			window.display();
		}

		if(chip.st > 0)
		{
			if(sound.getStatus() != Sound::Playing) sound.play();
		}
		else
			sound.stop();

		auto start = chrono::high_resolution_clock::now();
		this_thread::sleep_for(chrono::milliseconds(1));
		while(true)
		{
			auto elapsed = chrono::high_resolution_clock::now() - start;
			if(chrono::duration_cast<chrono::microseconds>(elapsed).count() > 1850) break;	//tick 540 times per second
		}

	}

	endwin();

	delete[] romBuffer;

	return 0;
}