#ifndef CHIP8CORE_H
#define CHIP8CORE_H

#include <string.h>	//memset, memcpy

#define MEM_SIZE 4096
#define ROM_MEM_OFFSET 0x200	//rom is loaded with a 512 byte offset to leave room for interpreter

const uint16_t FRAME_BUFFER_SIZE = 64 * 32;

//sprites for hexpad buttons
const uint8_t fontset[80] =
{ 
  0xF0, 0x90, 0x90, 0x90, 0xF0,	//0
  0x20, 0x60, 0x20, 0x20, 0x70,	//1
  0xF0, 0x10, 0xF0, 0x80, 0xF0,	//2
  0xF0, 0x10, 0xF0, 0x10, 0xF0,	//3
  0x90, 0x90, 0xF0, 0x10, 0x10,	//4
  0xF0, 0x80, 0xF0, 0x10, 0xF0,	//5
  0xF0, 0x80, 0xF0, 0x90, 0xF0,	//6
  0xF0, 0x10, 0x20, 0x40, 0x40,	//7
  0xF0, 0x90, 0xF0, 0x90, 0xF0,	//8
  0xF0, 0x90, 0xF0, 0x10, 0xF0,	//9
  0xF0, 0x90, 0xF0, 0x90, 0x90,	//A
  0xE0, 0x90, 0xE0, 0x90, 0xE0,	//B
  0xF0, 0x80, 0x80, 0x80, 0xF0,	//C
  0xE0, 0x90, 0x90, 0x90, 0xE0,	//D
  0xF0, 0x80, 0xF0, 0x80, 0xF0,	//E
  0xF0, 0x80, 0xF0, 0x80, 0x80	//F
};

//prototypes
class Chip8;
void execWithContext(Chip8*chip);

class Chip8
{
	public:
		//interface
		Chip8();
		void loadRom(uint8_t* rom, uint16_t romSize);
		void tick();

		//data
		uint8_t mem[MEM_SIZE];		//working memory
		uint8_t v[16];				//general purpose registers, called V0-Vf.  Vf is used to store the carry flag
		uint16_t stack[16];			//callstack
		uint8_t frameBuffer[64*32];	//frame buffer
		uint8_t keypad[16];			//input buffer
		uint16_t address;			//address register (usually called "i", but not here for the obvious reason)
		uint16_t ip;				//instruction pointer (also called "pc" program counter); rom begins at 512th byte
		uint16_t sp;				//stack pointer
		uint16_t opcode;			//current opcode
		uint16_t nextOpcode;		//can be displayed for debugging; not used for interpreter at all
		uint8_t dt;					//delay timer that decrements at 60Hz when nonzero.  used by programs for timing events
		uint8_t delayProgress;		//how many ticks have elapsed since last delay decrement.  at 540Hz, it takes 9 ticks
		uint8_t st;					//sound timer also decrements at 60Hz and tone is played while nonzero.  used by programs to make beeps and bloops
		uint8_t soundProgress;
		uint8_t drawFlag = false;	//set when framebuffer has been modified so the interface program can update its image

		//helper functions for opcode implementations
		inline uint8_t vX(){return v[(opcode >> 8 & 0x000F)];}				//returns contents of 'x' (second nibble of opcode) register
		inline uint8_t vY(){return v[(opcode >> 4 & 0x000F)];}				//returns contents of 'y' (third nibble of opcode) register
		inline void vX(uint8_t value){v[(opcode >> 8 & 0x000F)] = value;}	//sets contents of 'x' register
		inline void vY(uint8_t value){v[(opcode >> 4 & 0x000F)] = value;}	//sets contents of 'y' register
		inline uint8_t n(){return opcode & 0x000F;}							//returns least significant nibble of opcode
		inline uint8_t nn(){return opcode & 0x00FF;}						//returns least significant 2 nibbles of opcode
		inline uint16_t nnn(){return opcode & 0x0FFF;}						//take a guess ;)
};

Chip8::Chip8()
{
	//zero everything out
	memset(&mem, 0, MEM_SIZE);
	memset(&v, 0, 16);
	memset(&stack, 0, 16);
	memset(&frameBuffer, 0, 64*32);
	memset(&keypad, 0, 16);
	address = 0;
	ip = ROM_MEM_OFFSET;
	sp = 0;
	opcode = 0;
	dt = 0;	delayProgress = 0;
	st = 0;	soundProgress = 0;

	//load fontset
	memcpy(mem, fontset, 80);
}

void Chip8::loadRom(uint8_t* rom, uint16_t romSize)
{
	memcpy(mem+ROM_MEM_OFFSET, rom, romSize);
	nextOpcode = mem[ip] << 8 | mem[ip+1];
}

#define TICKS_PER_DEC 9
void Chip8::tick()
{
	drawFlag = false;

	//fetch
	opcode = mem[ip] << 8 | mem[ip+1];
	ip += 2;

	//execute
	execWithContext(this);

	//for display only
	nextOpcode = mem[ip] << 8 | mem[ip+1];

	//update timers
	if(dt > 0)
		delayProgress++;
		if(delayProgress == TICKS_PER_DEC)
		{
			dt--;
			delayProgress = 0;
		}
	if(st > 0)
	{
		soundProgress++;
		if(soundProgress == TICKS_PER_DEC)
		{
			st--;
			soundProgress = 0;
		}
	}
}

//////////////////////////
//opcode implementations//
//////////////////////////

//these can't be members of the Chip8 class because we need to be able to get pointers to them
//pointer to the Chip8 object is passed around for context

//0---	three seemingly unrelated ops?
	//0NNN	call a system subroutine.  not applicable in an emulator, so it does nothing
	void _sys(Chip8*chip){}

	//00E0	clear frame buffer
	void _clear(Chip8*chip)
	{
		memset(chip->frameBuffer, 0, FRAME_BUFFER_SIZE);
		chip->drawFlag = true;
	}

	//00EE	return from subroutine
	void _ret(Chip8*chip)
	{
		chip->sp--;
		chip->ip = chip->stack[chip->sp];
	}

	void _0___(Chip8*chip){
		switch(chip->opcode)
		{
			case 0x00E0:
				_clear(chip);
				break;
			case 0x00EE:
				_ret(chip);
				break;
			default:
				_sys(chip);
		}
	}

//1NNN	jump to address NNN
void _jump1(Chip8*chip){chip->ip = chip->nnn();}

//2NNN	call subroutine at NNN
void _call(Chip8*chip)
{
	chip->stack[chip->sp] = chip->ip;	//ip has already been incremented, so control will return to the instruction following this one
	chip->sp++;
	chip->ip = chip->nnn();
}

//3XNN	skip next instruction if register Vx equals NN
void _skipEqual1(Chip8*chip){if(chip->vX() == chip->nn()) chip->ip += 2;}

//4XNN	skip if register Vx does not equal NN
void _skipNotEqual1(Chip8*chip){if(chip->vX() != chip->nn()) chip->ip += 2;}

//5XY0	skip if register Vx equals register Vy
void _skipEqual2(Chip8*chip){if(chip->vX() == chip->vY()) chip->ip += 2;}

//6XNN	set register Vx to NN
void _loadConst(Chip8*chip){chip->vX(chip->nn());}

//7XNN	adds NN to register Vx
void _addConst(Chip8*chip){chip->vX(chip->vX() + chip->nn());}

//8XY-	9 arithmetic/logic operations
	//8XY0	sets register Vx to value in Vy
	void _copy(Chip8*chip){chip->vX(chip->vY());}

	//8XY1	sets Vx to Vx OR Vy
	void _or(Chip8*chip){chip->vX(chip->vX() | chip->vY());}

	//8XY2	sets Vx to Vx AND Vy
	void _and(Chip8*chip){chip->vX(chip->vX() & chip->vY());}

	//8XY3	sets Vx to Vx XOR Vy
	void _xor(Chip8*chip){chip->vX(chip->vX() ^ chip->vY());}

	//8XY4	sets Vx to Vx + Vy, setting Vf to 1 when there is a carry, 0 when not
	void _add(Chip8*chip)
	{
		uint16_t sum = chip->vX() + chip->vY();
		chip->vX((uint8_t)sum);
		chip->v[0xF] = sum > 0xFF;
	}

	//8XY5	sets Vx to Vx - Vy, setting Vf to 0 when there is a borrow, 1 when not
	void _sub1(Chip8*chip)
	{
		chip->v[0xF] = chip->vX() >= chip->vY();
		chip->vX(chip->vX() - chip->vY());
	}

	//8XY6	shifts Vx right by one.  Vf is set to the LSB of Vx before the shift
	//HEADACHE!!!  Wikipedia docs are incorrect for both left and right shifts!
	//At the time of writing, they claim this should be Vx = Vy = Vy >> 1
	//really it's just Vx = Vx >> 1.  Whoever wrote that should feel the burn of shame.
	void _shiftRight(Chip8*chip)
	{
		chip->v[0xF] = chip->vX() & 1;
		chip->vX(chip->vX() >> 1);
	}

	//8XY7	sets Vx to Vy - Vx, setting Vf to 0 when there is a borrow, 1 when not
	void _sub2(Chip8*chip)
	{
		chip->v[0xF] = chip->vY() >= chip->vX();
		chip->vX(chip->vY() - chip->vX());
	}

	//8XYE	shifts Vx left by one.  Vf is set to the MSB of Vx before the shift
	void _shiftLeft(Chip8*chip)
	{
		chip->v[0xF] = chip->vX() >> 7 & 1;	//necessary?  should never be arithmetic shift
		chip->vX(chip->vX() << 1);
	}

	void (*_8XY_CALLTABLE[15])(Chip8*) = {_copy,_or,_and,_xor,_add,_sub1,_shiftRight,_sub2,0,0,0,0,0,0,_shiftLeft};	//for some reason there are several places skipped, so array is padded
	void _8XY_(Chip8*chip){_8XY_CALLTABLE[chip->opcode & 0x000F](chip);}

//9XY0	skips the next instruction if Vx does not equal register Vy
void _skipNotEqual2(Chip8*chip){if(chip->vX() != chip->vY()) chip->ip += 2;}

//ANNN	sets address register to NNN
void _loadAddress(Chip8*chip){chip->address = chip->nnn();}

//BNNN	jumps to address stored in V0 + NNN
void _jump2(Chip8*chip){chip->ip = chip->v[0] + chip->nnn();}

//CXNN	sets Vx to a random byte & NN
void _random(Chip8*chip){chip->vX((std::rand() % 256) & chip->nn());}

//DXYN	draws a sprite at (Vx, Vy) with width of 8px and height of Npx.  sprite is pointed to by address register.
//this is done by XORing the framebuffer bytes with the sprite bytes.  Vf is set to 1 if this causes any on FB bits to become off, 0 otherwise.
void _drawSprite(Chip8*chip)
{
	chip->v[0xF] = 0;
	uint8_t spriteHeight = chip->n();
	for(uint8_t i = 0; i < spriteHeight; i++)
	{
		for(uint8_t j = 0; j < 8; j++)
		{
			uint16_t fbIndex = (chip->vY() + i) * 64 + chip->vX() + j;
			//some programs (ex. vers) draw outside the framebuffer sometimes.
			//this seems to have no effect, but we need to make sure not to overwrite anything else
			if(fbIndex >= 64*32) continue;

			bool wasOn = chip->frameBuffer[fbIndex];
			chip->frameBuffer[fbIndex] = chip->frameBuffer[fbIndex]
				^ (chip->mem[chip->address + i] >> (7-j) & 1);
			if(wasOn && !chip->frameBuffer[fbIndex]) chip->v[0xF] = 1;
		}
	}
	chip->drawFlag = true;
}
						
//EX--	two ops for detecting input
	//EX9E	skip next instruction if key indexed by Vx is being pressed
	void _skipKey(Chip8*chip){if(chip->keypad[chip->vX()]) chip->ip += 2;}

	//EXA1	skip next instruction if key is not pressed
	void _skipNotKey(Chip8*chip){if(!(chip->keypad[chip->vX()])) chip->ip += 2;}	//TODO: unnecessary parens?

	void _EX__(Chip8*chip){if((chip->opcode & 0x00FF) == 0x009E) _skipKey(chip); else _skipNotKey(chip);}

//FX--
	//FX0-
		//FX07	copy the delay timer value into Vx
		void _getDelay(Chip8*chip){chip->vX(chip->dt);}

		//FX0A	pause execution until a key is pressed, then store it in Vx
		void _waitKey(Chip8*chip)
		{
			for(uint8_t i = 0; i < 16; i++)
				if(chip->keypad[i])
				{
					chip->vX(i);
					return;
				}

			chip->ip -= 2;
		}

		void _FX0_(Chip8*chip){if((chip->opcode & 0x000F) == 0x0007) _getDelay(chip); else _waitKey(chip);}

	//FX1-
		//FX15	set delay timer to value in Vx
		void _setDelay(Chip8*chip){chip->dt = chip->vX();}

		//FX18	set sound timer to value in Vx
		void _setSound(Chip8*chip){chip->st = chip->vX();}

		//FX1E	adds Vx to address register
		void _augmentAddress(Chip8*chip){chip->address += chip->vX();}

		void (*_FX1_TABLE[15])(Chip8*) = {0,0,0,0,0,_setDelay,0,0,_setSound,0,0,0,0,0,_augmentAddress};
		void _FX1_(Chip8*chip){_FX1_TABLE[chip->opcode & 0x000F](chip);}

	//FX29	sets address register to address of font sprite with index Vx
	void _fontSprite(Chip8*chip){chip->address = chip->vX() * 5;}

	//FX33	stores binary-coded decimal representation of Vx in address register
	void _bcd(Chip8*chip)
	{
		uint8_t value = chip->vX();
		uint8_t hundreds = value / 100;
		value -= 100 * hundreds;
		uint8_t tens = value / 10;
		value -= 10 * tens;

		chip->mem[chip->address+0] = hundreds;
		chip->mem[chip->address+1] = tens;
		chip->mem[chip->address+2] = value;
	}

	//FX55	writes values of V0-Vx (inclusive) to memory, starting at address in address register
	//note: FX55 and FX65 are the only instructions where the registers indexed by nibble 2 are not actually accessed
	void _store(Chip8*chip)
	{
		uint8_t copyLength = chip->opcode >> 8 & 0x000F;
		for(int i = 0; i <= copyLength; i++)
		{
			chip->mem[chip->address+i] = chip->v[i];
		}
	}

	//FX65	opposite of above; copies values from memory into registers
	void _read(Chip8*chip)
	{
		uint8_t copyLength = chip->opcode >> 8 & 0x000F;
		for(int i = 0; i <= copyLength; i++)
		{
			chip->v[i] = chip->mem[chip->address+i];
		}
	}

	void (*_FX__CALLTABLE[7])(Chip8*) = {_FX0_,_FX1_,_fontSprite,_bcd,0,_store,_read};
	void _FX__(Chip8*chip){_FX__CALLTABLE[chip->opcode >> 4 & 0x000F](chip);}

void (*MSN_CALLTABLE[16])(Chip8*) = {_0___,_jump1,_call,_skipEqual1,_skipNotEqual1,_skipEqual2,_loadConst,_addConst,
	_8XY_,_skipNotEqual2,_loadAddress,_jump2,_random,_drawSprite,_EX__,_FX__};
void execWithContext(Chip8*chip){MSN_CALLTABLE[chip->opcode>>12](chip);}

#endif
